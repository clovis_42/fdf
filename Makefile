# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: clmenega <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/07/31 17:41:24 by clmenega          #+#    #+#              #
#    Updated: 2019/10/06 19:07:25 by clmenega         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
FLAGS = -Wall -Wextra -Werror
SRCS =	main.c		\
		get_map.c	\
		ft_split_n_convert.c \
		print_array.c \
		get_param.c \
		rotation.c\
		draw.c \
		draw_line.c \
		refresh.c \
		wu_line.c \
		cone.c \
		key_press.c \
		key_angle.c \

OBJS = $(SRCS:%.c=%.o)

INCLU_LIB_PATH = $(addprefix $(LIB_PATH), $(INCLU_PATH))

INCLU_PATH = includes

LIB_PATH = lib/

SRCS_PATH = srcs/

OBJS_PATH = objs/

SRCS_FILES = $(addprefix $(SRCS_PATH), $(SRCS))

OBJS_FILES = $(addprefix $(OBJS_PATH), $(OBJS))

LIBFT = $(LIB_PATH)libft.a

MLX_LIB_PATH = /usr/local/lib

MLX_FLAG = -lmlx -framework OpenGL -framework AppKit

all : $(NAME)

$(OBJS_PATH) :
	mkdir $(OBJS_PATH)

$(LIBFT) : $(LIB_PATH)Makefile
	make -C $(LIB_PATH)

$(NAME) : $(SRCS_FILES) $(LIBFT) Makefile
	make objects
	gcc -I$(INCLU_PATH) -I$(INCLU_LIB_PATH) $(FLAGS) -o $@ $(OBJS_FILES) $(LIBFT) -L $(MLX_LIB_PATH) $(MLX_FLAG)

objects : $(OBJS_PATH) $(OBJS_FILES)

$(OBJS_PATH)%.o : $(SRCS_PATH)%.c
	gcc -I$(INCLU_PATH) -I$(INCLU_LIB_PATH) -o $@ $(FLAGS) -c $< 

clean :
	rm -rf $(OBJS_FILES)
	rm -rf $(OBJS_PATH)
	make clean -C $(LIB_PATH)

fclean : clean
	rm -fr $(NAME)
	make fclean -C $(LIB_PATH)

re : fclean all

.PHONY: all clean fclean re

#.SILENT:
