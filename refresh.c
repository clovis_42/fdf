/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   refresh.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 18:20:09 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/09 17:45:01 by behiraux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	refresh(t_prm *prm)
{
// application modif touche
	bzero(prm.img.img_ptr);
	draw(prm);
	mlx_put_image_to_window(prm.mlx_ptr, prm.win.win_ptr,\
	prm.img.img_ptr, 0, 0);
	return (1);
}
