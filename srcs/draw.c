/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 06:39:59 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/06 23:23:02 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdio.h>

int		offset(t_prm *prm)
{
	prm->draw.centre.x += prm->draw.offset.x;
	prm->draw.centre.y += prm->draw.offset.y;
	prm->draw.a.x += prm->draw.offset.x;
	prm->draw.b.x += prm->draw.offset.x;
	prm->draw.a.y += prm->draw.offset.y;
	prm->draw.b.y += prm->draw.offset.y;
	return (1);
}

int		scale(t_prm *prm)
{
	prm->draw.a.x *= prm->img.spacing;
	prm->draw.b.x *= prm->img.spacing;
	prm->draw.a.y *= prm->img.spacing;
	prm->draw.b.y *= prm->img.spacing;
	prm->draw.a.z *= prm->img.spacing * prm->img.factor_z;
	prm->draw.b.z *= prm->img.spacing * prm->img.factor_z;
	return (1);
}

int		set_coord(int x, int y, t_prm *prm, int code)
{
	prm->draw.a.z = (double)prm->map.map[y][x];
	prm->draw.a.x = (double)x;
	prm->draw.a.y = (double)y;
	code == 1 ? x++ : y++;
	prm->draw.b.z = (double)prm->map.map[y][x];
	prm->draw.b.x = (double)x;
	prm->draw.b.y = (double)y;
	scale(prm);
	if (prm->draw.cone)
		conique(prm);
	rotation(prm);
	offset(prm);
	return (SUCSSES);
}

int		draw_all(int x, int y, t_prm *prm)
{
	y = 0;
	while (y < prm->map.height)
	{
		x = 0;
		while (x < prm->map.width)
		{
			if (x + 1 != prm->map.width)
			{
				set_coord(x, y, prm, 1);
				draw_line(prm);
			}
			if (y + 1 != prm->map.height)
			{
				set_coord(x, y, prm, 2);
				draw_line(prm);
			}
			x++;
		}
		y++;
	}
	return (1);
}

int		draw(t_prm *prm)
{
	int	x;
	int y;

	y = 0;
	x = 0;
	draw_all(x, y, prm);
	return (1);
}
