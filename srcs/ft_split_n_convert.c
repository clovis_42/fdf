/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_n_convert.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 21:14:20 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/08 23:51:31 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_count_w(char const *s, char c)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (i < (int)ft_strlen(s))
	{
		while (s[i] && s[i] == c)
			i++;
		if (s[i] && s[i] != c)
		{
			while (s[i] && s[i] != c)
			{
				if (ft_isdigit(s[i]) == 0)
					return (0);
				i++;
			}
			count++;
		}
	}
	return (count);
}

static int	ft_convert(char *str, int *index, char c)
{
	int i;

	i = 0;
	if (str[i] == '-')
		i++;
	while (str[i] && (str[i] != c))
	{
		if (ft_isdigit(str[i]) == 0)
		{
			*index = 0;
			break ;
		}
		i++;
	}
	return (ft_atoi(str));
}

static int	ft_nextnb(const char *str, char c, int *index)
{
	int		i;
	int		y;
	char	buff[12];

	y = 0;
	ft_bzero(buff, 12);
	i = *index;
	while (str[i] == c)
		i++;
	ft_memccpy(buff, &str[i], c, ft_strlen(&str[i]));
	while (str[i] && str[i] != c)
		i++;
	*index = i;
	return (ft_convert(buff, index, c));
}

size_t		ft_split_n_convert(char *s, char c, int **tab)
{
	int		*ret;
	int		i;
	int		y;
	int		count;

	if (!s)
		return (0);
	if ((count = ft_count_w(s, c)) == 0)
		return (0);
	y = -1;
	i = 0;
	if (!(ret = (int*)ft_memalloc(count * sizeof(int))))
		return (0);
	while (++y < count)
	{
		ret[y] = ft_nextnb(s, c, &i);
		if (i == 0)
		{
			free(ret);
			return (0);
		}
	}
	*tab = ret;
	free(s);
	return (count);
}
