/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_array.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 20:47:23 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/08 23:35:26 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_putndl(char *str)
{
	ft_putstr(str);
	ft_putchar('\n');
}

void	print_cmd(void)
{
	ft_putndl("how to fdf:");
	ft_putndl("move object : arrows");
	ft_putndl("zoom : + (num)");
	ft_putndl("unzoom : - (num)");
	ft_putndl("y rotation : 4 and 6 (num)");
	ft_putndl("x rotation : 5 and 8 (num)");
	ft_putndl("z rotation : 9 and 7 (num)");
	ft_putndl("			 p ");
	ft_putndl("move vanishing point : l ; ' ");
	ft_putndl("reset : space");
	ft_putndl("change hight : page up, page down, end, home");
	ft_putndl("chage type of line : 1");
	ft_putndl("chage type of proj : 2");
}

void	print_array(int **array, int size)
{
	int i;
	int y;

	i = 0;
	while (array[i])
	{
		y = 0;
		while (y < size)
		{
			ft_putnbr(array[i][y]);
			ft_putchar(' ');
			y++;
		}
		ft_putchar('\n');
		i++;
	}
}
