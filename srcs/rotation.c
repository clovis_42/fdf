/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 18:09:55 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/06 21:51:48 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		rot_x(t_prm *prm)
{
	double z;
	double y;
	double cosinus;
	double sinus;

	cosinus = cos(prm->draw.agl.x);
	sinus = sin(prm->draw.agl.x);
	y = prm->draw.a.y * cosinus - prm->draw.a.z * sinus;
	z = prm->draw.a.z * cosinus + prm->draw.a.z * sinus;
	prm->draw.a.z = z;
	prm->draw.a.y = y;
	y = prm->draw.b.y * cosinus - prm->draw.b.z * sinus;
	z = prm->draw.b.z * cosinus + prm->draw.b.z * sinus;
	prm->draw.b.z = z;
	prm->draw.b.y = y;
	return (1);
}

int		rot_y(t_prm *prm)
{
	double x;
	double z;
	double cosinus;
	double sinus;

	cosinus = cos(prm->draw.agl.y);
	sinus = sin(prm->draw.agl.y);
	x = prm->draw.a.x * cosinus + prm->draw.a.z * sinus;
	z = prm->draw.a.z * cosinus - prm->draw.a.x * sinus;
	prm->draw.a.x = x;
	prm->draw.a.z = z;
	x = prm->draw.b.x * cosinus + prm->draw.b.z * sinus;
	z = prm->draw.b.z * cosinus - prm->draw.b.x * sinus;
	prm->draw.b.x = x;
	prm->draw.b.z = z;
	return (1);
}

int		rot_z(t_prm *prm)
{
	double x;
	double y;
	double cosinus;
	double sinus;

	cosinus = cos(prm->draw.agl.z);
	sinus = sin(prm->draw.agl.z);
	x = prm->draw.a.x * cosinus - prm->draw.a.y * sinus;
	y = prm->draw.a.y * cosinus + prm->draw.a.x * sinus;
	prm->draw.a.x = x;
	prm->draw.a.y = y;
	x = prm->draw.b.x * cosinus - prm->draw.b.y * sinus;
	y = prm->draw.b.y * cosinus + prm->draw.b.x * sinus;
	prm->draw.b.x = x;
	prm->draw.b.y = y;
	return (1);
}

int		rotation(t_prm *prm)
{
	rot_x(prm);
	rot_y(prm);
	rot_z(prm);
	return (SUCSSES);
}
