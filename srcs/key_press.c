/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_press.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/06 18:11:46 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/09 17:13:35 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			reset(t_prm *prm)
{
	prm->draw.agl.x = 0;
	prm->draw.agl.y = 0;
	prm->draw.agl.z = 0;
	prm->draw.offset.x = WIN_WIDTH / 3;
	prm->draw.offset.y = WIN_HEIGHT / 3;
	(prm->draw.line_type = 0);
	prm->draw.vanish.x = -1;
	prm->draw.vanish.y = -5;
	prm->img.factor_z = 0.5;
	prm->draw.factor_v = 0.5;
	prm->draw.line_type = 1;
	prm->img.spacing = prm->img.save_spacing;
	prm->draw.cone == 1 ? (prm->img.factor_z = 0.1) : 0;
}

static void		perspective_key(int keycode, t_prm *prm)
{
	if (keycode == 116 && !(prm->draw.cone == 1) && prm->img.factor_z < INT_MAX)
		prm->img.factor_z += 0.1;
	if (keycode == 121 && !(prm->draw.cone == 1) && prm->img.factor_z > INT_MIN)
		prm->img.factor_z -= 0.1;
	if (keycode == 115 && !(prm->draw.cone == 1) && prm->img.factor_z < INT_MAX)
		prm->img.factor_z += 0.001;
	if (keycode == 119 && !(prm->draw.cone == 1) && prm->img.factor_z > INT_MIN)
		prm->img.factor_z -= 0.001;
	if (keycode == 37 && prm->draw.vanish.x > INT_MIN)
		prm->draw.vanish.x -= 1;
	if (keycode == 39 && prm->draw.vanish.x < INT_MAX)
		prm->draw.vanish.x += 1;
	if (keycode == 41 && prm->draw.vanish.x < INT_MAX)
		prm->draw.vanish.y += 1;
	if (keycode == 35 && prm->draw.vanish.x > INT_MIN)
		prm->draw.vanish.y -= 1;
	if (keycode == 37 && prm->draw.vanish.x > INT_MIN)
		prm->draw.vanish.x -= 1;
	if (keycode == 39 && prm->draw.vanish.x < INT_MAX)
		prm->draw.vanish.x += 1;
	prm->draw.cone == 1 ? (prm->img.factor_z = 0.1) : 0;
}

static void		basic_key(int keycode, t_prm *prm)
{
	if (keycode == 53)
		clean_out(prm);
	if (keycode == 49)
		reset(prm);
	if (keycode == 126)
		prm->draw.offset.y -= 5;
	if (keycode == 125)
		prm->draw.offset.y += 5;
	if (keycode == 124)
		prm->draw.offset.x += 5;
	if (keycode == 123)
		prm->draw.offset.x -= 5;
	if (keycode == 69 && prm->draw.cone == 0)
		prm->img.spacing += 1;
	if (keycode == 78 && prm->img.spacing > 1 && prm->draw.cone == 0)
		prm->img.spacing -= 1;
	if (keycode == 18)
		prm->draw.line_type == 0 ? (prm->draw.line_type = 1) : \
			(prm->draw.line_type = 0);
	if (keycode == 19)
	{
		prm->draw.cone == 0 ? (prm->draw.cone = 1) : \
						(prm->draw.cone = 0);
		reset(prm);
	}
}

int				key_press(int keycode, t_prm *prm)
{
	key_angle(keycode, prm);
	perspective_key(keycode, prm);
	basic_key(keycode, prm);
	refresh(prm);
	return (0);
}
