/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   refresh.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 18:20:09 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/08 21:51:11 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		refresh(t_prm *prm)
{
	bzero(prm->img.data, (IMG_WIDTH * IMG_HEIGHT * 4));
	draw(prm);
	mlx_clear_window(prm->mlx_ptr, prm->win.win_ptr);
	mlx_put_image_to_window(prm->mlx_ptr, prm->win.win_ptr, \
			prm->img.img_ptr, 0, 0);
	return (1);
}
