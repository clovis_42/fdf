/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_angle.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2029/10/06 18:15:29 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/06 22:10:27 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	next(int keycode, t_prm *prm)
{
	if (keycode == 86)
	{
		prm->draw.agl.y -= 0.02 * M_PI;
		if (prm->draw.agl.y == -2 * M_PI)
			prm->draw.agl.y = 0;
	}
	if (keycode == 92)
	{
		prm->draw.agl.z += 0.02 * M_PI;
		if (prm->draw.agl.z == 2 * M_PI)
			prm->draw.agl.z = 0;
	}
	if (keycode == 89)
	{
		prm->draw.agl.z -= 0.02 * M_PI;
		if (prm->draw.agl.z == -2 * M_PI)
			prm->draw.agl.z = 0;
	}
}

void		key_angle(int keycode, t_prm *prm)
{
	if (keycode == 91)
	{
		prm->draw.agl.x += 0.02 * M_PI;
		if (prm->draw.agl.x == 2 * M_PI)
			prm->draw.agl.x = 0;
	}
	if (keycode == 87)
	{
		prm->draw.agl.x -= 0.02 * M_PI;
		if (prm->draw.agl.x == -2 * M_PI)
			prm->draw.agl.x = 0;
	}
	if (keycode == 88)
	{
		prm->draw.agl.y += 0.02 * M_PI;
		if (prm->draw.agl.y == 2 * M_PI)
			prm->draw.agl.y = 0;
	}
	next(keycode, prm);
}
