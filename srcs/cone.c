/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 18:09:54 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/06 23:27:38 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	conique(t_prm *prm)
{
	double x;
	double y;

	x = -prm->draw.a.x + prm->draw.vanish.x;
	y = -prm->draw.a.y + prm->draw.vanish.y;
	prm->draw.a.x += x * prm->draw.a.z * (prm->img.factor_z / 10);
	prm->draw.a.y += y * prm->draw.a.z * (prm->img.factor_z / 10);
	x = -prm->draw.b.x + prm->draw.vanish.x;
	y = -prm->draw.b.y + prm->draw.vanish.y;
	prm->draw.b.x += x * prm->draw.b.z * (prm->img.factor_z / 10);
	prm->draw.b.y += y * prm->draw.b.z * (prm->img.factor_z / 10);
}
