/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 15:52:05 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/06 20:16:23 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		put_pixel(int x, int y, t_prm *prm)
{
	unsigned long	index;
	char			*ptr;

	ptr = prm->img.data;
	index = (((y * prm->img.width) + x - 1) * (prm->img.bpp / 8));
	if (prm->img.endian == 1)
	{
		ptr[index] = prm->img.color.o;
		ptr[++index] = prm->img.color.b;
		ptr[++index] = prm->img.color.r;
		ptr[++index] = prm->img.color.g;
	}
	else
	{
		ptr[index] = prm->img.color.g;
		ptr[++index] = prm->img.color.r;
		ptr[++index] = prm->img.color.b;
		ptr[++index] = prm->img.color.o;
	}
	return (0);
}

int		v_line(double *d, double *coord, double *inc, t_prm *prm)
{
	int cumul;
	int	i;

	cumul = d[1] / 2;
	i = -1;
	while (++i < d[1])
	{
		coord[1] += inc[1];
		cumul += d[0];
		if (cumul >= d[1])
		{
			cumul -= d[1];
			coord[0] += inc[0];
		}
		if (coord[0] >= 0 && coord[0] <= IMG_WIDTH && \
				coord[1] >= 0 && coord[1] <= IMG_HEIGHT)
			put_pixel((int)coord[0], (int)coord[1], prm);
	}
	return (1);
}

int		h_line(double *d, double *coord, double *inc, t_prm *prm)
{
	int cumul;
	int	i;

	cumul = d[0] / 2;
	i = -1;
	while (++i < d[0])
	{
		coord[0] += inc[0];
		cumul += d[1];
		if (cumul >= d[0])
		{
			cumul -= d[0];
			coord[1] += inc[1];
		}
		if (coord[0] >= 0 && coord[0] <= IMG_WIDTH && \
			coord[1] >= 0 && coord[1] <= IMG_HEIGHT)
			put_pixel((int)coord[0], (int)coord[1], prm);
	}
	return (1);
}

int		bresenham(t_prm *prm)
{
	double	d[2];
	double	coord[2];
	double	inc[2];
	double	y;
	double	x;

	y = prm->draw.a.y;
	x = prm->draw.a.x;
	coord[0] = x;
	coord[1] = y;
	if (prm->draw.b.x - x > 0)
		(inc[0] = 1);
	else
		(inc[0] = -1);
	if (prm->draw.b.y - y > 0)
		(inc[1] = 1);
	else
		(inc[1] = -1);
	d[0] = fabs(prm->draw.b.x - prm->draw.a.x);
	d[1] = fabs(prm->draw.b.y - prm->draw.a.y);
	if (coord[0] >= 0 && coord[0] < IMG_WIDTH && \
		coord[1] >= 0 && coord[1] < IMG_HEIGHT)
		put_pixel((int)coord[0], (int)coord[1], prm);
	d[0] > d[1] ? h_line(d, coord, inc, prm) : v_line(d, coord, inc, prm);
	return (1);
}

int		draw_line(t_prm *prm)
{
	if (prm->draw.line_type == 0)
		bresenham(prm);
	else
		wu_line(prm);
	return (0);
}
