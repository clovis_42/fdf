/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wu_line.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 17:39:00 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/06 21:52:23 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		put_wu_pixel(double x, double y, double c, t_prm *prm)
{
	unsigned long	index;
	int				o;
	char			*ptr;

	o = (int)(c * 255);
	o = 255 - o;
	ptr = prm->img.data;
	if (!(x >= 0 && x <= IMG_WIDTH && y >= 0 && y <= IMG_HEIGHT))
		return (0);
	index = ((((int)y * prm->img.width) + (int)x - 1) * (prm->img.bpp / 8));
	if (prm->img.endian == 1)
	{
		ptr[index] = o;
		ptr[++index] = prm->img.color.b;
		ptr[++index] = prm->img.color.r;
		ptr[++index] = prm->img.color.g;
	}
	else
	{
		ptr[index] = prm->img.color.g;
		ptr[++index] = prm->img.color.r;
		ptr[++index] = prm->img.color.b;
		ptr[++index] = o;
	}
	return (0);
}

void	first_point(t_prm *prm)
{
	double tmp;

	prm->wu.gap = 1 - modf((prm->draw.a.x + 0.5), &(prm->wu.end.x));
	prm->wu.end.y = prm->draw.a.y + prm->wu.gradient *\
					(prm->wu.end.x - prm->draw.a.x);
	prm->wu.pxl1.x = prm->wu.end.x;
	modf(prm->wu.end.y, &(prm->wu.pxl1.y));
	if (prm->wu.steep)
	{
		put_wu_pixel(prm->wu.pxl1.y, prm->wu.pxl1.x,\
				1 - modf(prm->wu.end.y, &tmp), prm);
		put_wu_pixel(prm->wu.pxl1.y + 1, prm->wu.pxl1.x, \
				modf(prm->wu.end.y, &tmp), prm);
	}
	else
	{
		put_wu_pixel(prm->wu.pxl1.x, prm->wu.pxl1.y,\
				1 - modf(prm->wu.end.y, &tmp), prm);
		put_wu_pixel(prm->wu.pxl1.x, prm->wu.pxl1.y + 1, \
				modf(prm->wu.end.y, &tmp), prm);
	}
	prm->wu.intery = prm->wu.end.y + prm->wu.gradient;
}

void	second_point(t_prm *prm)
{
	double tmp;

	prm->wu.gap = 1 - modf((prm->draw.b.x + 0.5), &(prm->wu.end.x));
	prm->wu.end.y = prm->draw.b.y + prm->wu.gradient *\
					(prm->wu.end.x - prm->draw.b.x);
	prm->wu.pxl2.x = prm->wu.end.x;
	modf(prm->wu.end.y, &(prm->wu.pxl2.y));
	if (prm->wu.steep)
	{
		put_wu_pixel(prm->wu.pxl2.y, prm->wu.pxl2.x,\
				1 - modf(prm->wu.end.y, &tmp), prm);
		put_wu_pixel(prm->wu.pxl2.y + 1, prm->wu.pxl2.x, \
				modf(prm->wu.end.y, &tmp), prm);
	}
	else
	{
		put_wu_pixel(prm->wu.pxl2.x, prm->wu.pxl2.y,\
				1 - modf(prm->wu.end.y, &tmp), prm);
		put_wu_pixel(prm->wu.pxl2.x, prm->wu.pxl2.y + 1, \
				modf(prm->wu.end.y, &tmp), prm);
	}
}

void	wu_loop(t_prm *prm, double a)
{
	double	x;
	double	b;

	if (prm->wu.steep)
	{
		x = prm->wu.pxl1.x;
		while (++x <= prm->wu.pxl2.x - 1)
		{
			b = modf(prm->wu.intery, &a);
			put_wu_pixel(a, x, 1 - b, prm);
			put_wu_pixel(a + 1, x, b, prm);
			prm->wu.intery += prm->wu.gradient;
		}
	}
	else
	{
		x = prm->wu.pxl1.x;
		while (++x <= prm->wu.pxl2.x - 1)
		{
			b = modf(prm->wu.intery, &a);
			put_wu_pixel(x, a, 1 - b, prm);
			put_wu_pixel(x, a + 1, b, prm);
			prm->wu.intery += prm->wu.gradient;
		}
	}
}

int		wu_line(t_prm *prm)
{
	double a;

	a = 0;
	fabs(prm->draw.b.y - prm->draw.a.y) > fabs(prm->draw.b.x - prm->draw.a.x) \
		? (prm->wu.steep = 1) :\
		(prm->wu.steep = 0);
	if (prm->wu.steep)
	{
		ft_fswap(&prm->draw.a.x, &prm->draw.a.y);
		ft_fswap(&prm->draw.b.x, &prm->draw.b.y);
	}
	if (prm->draw.a.x > prm->draw.b.x)
	{
		ft_fswap(&prm->draw.a.x, &prm->draw.b.x);
		ft_fswap(&prm->draw.a.y, &prm->draw.b.y);
	}
	prm->wu.d.x = prm->draw.b.x - prm->draw.a.x;
	prm->wu.d.y = prm->draw.b.y - prm->draw.a.y;
	prm->wu.d.x == 0 ? (prm->wu.gradient = 1) :\
			(prm->wu.gradient = (prm->wu.d.y / prm->wu.d.x));
	first_point(prm);
	second_point(prm);
	wu_loop(prm, a);
	return (0);
}
