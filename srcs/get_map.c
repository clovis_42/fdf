/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 13:50:08 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/08 23:55:35 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_list			*lstnew(int *content, int content_size)
{
	t_list	*list;

	if ((list = (t_list*)ft_memalloc(sizeof(t_list))))
	{
		if (content == NULL)
		{
			list->content = NULL;
			list->content_size = 0;
			list->next = NULL;
		}
		else
		{
			list->content = content;
			list->content_size = content_size;
			list->next = NULL;
		}
	}
	else
		list = NULL;
	return (list);
}

static int		get_tab(t_list *lst, int ***tab)
{
	t_list	*tmp;
	t_list	*rm;
	int		i;

	i = 0;
	tmp = lst;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	if (!(*tab = (int**)ft_memalloc((i + 1) * sizeof(int*))))
		return (0);
	i = 0;
	tmp = lst;
	while (tmp)
	{
		tab[0][i] = (int*)(tmp->content);
		rm = tmp;
		tmp = tmp->next;
		free(rm);
		i++;
	}
	return (SUCSSES);
}

static int		get_list(int fd, t_list **lst, size_t *ctrl)
{
	size_t	size;
	int		*tab;
	char	*str;
	t_list	*tmp;

	*lst = NULL;
	*ctrl = 0;
	while (get_next_line(fd, &str))
	{
		size = ft_split_n_convert(str, ' ', &tab);
		(*ctrl == 0) ? (*ctrl = size) : 0;
		if (size == 0 || size != *ctrl)
			return (ERROR);
		if (*lst == NULL)
		{
			*lst = lstnew(tab, size * sizeof(*tab));
			tmp = *lst;
		}
		else
		{
			tmp->next = lstnew(tab, size * sizeof(*tab));
			tmp = tmp->next;
		}
	}
	return (SUCSSES);
}

static int		good_fd(char *file)
{
	int fd;

	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		ft_putstr("invalid file descrptor");
		exit(1);
	}
	return (fd);
}

int				get_map(char *file, int ***tab, size_t *size)
{
	int		fd;
	t_list	*lst;

	fd = good_fd(file);
	if (get_list(fd, &lst, size) == ERROR || *size == 0)
		ft_error("invalid file");
	get_tab(lst, tab);
	return (SUCSSES);
}
