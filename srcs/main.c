/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 17:54:16 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/08 23:35:06 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	clean_out(t_prm *prm)
{
	free_map(prm->map.map);
	mlx_destroy_image(prm->mlx_ptr, prm->img.img_ptr);
	mlx_destroy_window(prm->mlx_ptr, prm->win.win_ptr);
	exit(1);
}

void	free_map(int **map)
{
	size_t i;

	i = -1;
	while (map[++i])
		free(map[i]);
	free(map);
}

void	ft_error(char *str)
{
	ft_putstr("error\n");
	ft_putstr(str);
	ft_putchar('\n');
	exit(1);
}

int		main(int ac, char **av)
{
	int		**tab;
	size_t	size;
	t_prm	prm;

	if (ac != 2)
		ft_error("nombre argu");
	get_map(av[1], &tab, &size);
	print_array(tab, size);
	print_cmd();
	init(&prm, tab, size);
	mlx_hook(prm.win.win_ptr, 2, 0, key_press, &prm);
	refresh(&prm);
	mlx_put_image_to_window(prm.mlx_ptr, prm.win.win_ptr,\
			prm.img.img_ptr, 0, 0);
	mlx_loop(prm.mlx_ptr);
	return (0);
}
