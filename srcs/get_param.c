/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_prm.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 01:05:12 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/06 20:22:39 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdio.h>

static void		set_color(t_prm *prm)
{
	prm->img.color.o = (char)0;
	prm->img.color.g = (char)255;
	prm->img.color.r = (char)255;
	prm->img.color.b = (char)255;
}

static void		init_img(t_prm *prm, int size, int **tab)
{
	int i;
	int y;

	i = 0;
	while (tab[i])
		i++;
	prm->map.height = i;
	prm->img.img_ptr = mlx_new_image(prm->mlx_ptr, IMG_WIDTH, IMG_HEIGHT);
	prm->img.data = mlx_get_data_addr(prm->img.img_ptr, (&(prm->img.bpp)), \
			(&(prm->img.bpl)), &(prm->img.endian));
	prm->draw.centre.x = 0;
	prm->draw.centre.y = 0;
	prm->draw.centre.z = 0;
	prm->draw.cone = 0;
	y = i;
	i = 0;
	while (((i * size) < (IMG_WIDTH - 200)) && ((i * y) < (IMG_HEIGHT - 200)))
		i++;
	prm->img.save_spacing = (double)i / 10;
	set_color(prm);
	reset(prm);
}

int				init(t_prm *prm, int **tab, int size)
{
	prm->mlx_ptr = mlx_init();
	prm->map.width = size;
	prm->img.width = IMG_WIDTH;
	prm->img.height = IMG_HEIGHT;
	prm->map.map = tab;
	prm->win.win_ptr = mlx_new_window(prm->mlx_ptr, WIN_WIDTH, \
			WIN_HEIGHT, "FdF");
	init_img(prm, size, prm->map.map);
	return (SUCSSES);
}
