/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 11:51:58 by clmenega          #+#    #+#             */
/*   Updated: 2019/10/08 23:27:47 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# define SUCSSES	1
# define FAILURE	0
# define TRUE		1
# define FALSE		0
# define ERROR		-1
# define END		2
# define NEXT_Y		3
# define NEXT_X		4
# define WIN_WIDTH	1500
# define WIN_HEIGHT	1200
# define IMG_WIDTH	5400
# define IMG_HEIGHT	4000
# define INT_MAX	2147483647
# define INT_MIN	-2147483648
# include "libft.h"
# include "libft.h"
# include <fcntl.h>
# include <mlx.h>
# include <stdlib.h>
# include <math.h>

typedef	struct		s_point
{
	double			x;
	double			y;
	double			z;
}					t_point;

typedef	struct		s_coord
{
	int				x;
	int				y;
}					t_coord;

typedef struct		s_color
{
	char			o;
	char			b;
	char			r;
	char			g;
}					t_color;

typedef	struct		s_map
{
	int				height;
	int				width;
	int				**map;
}					t_map;

typedef	struct		s_img
{
	t_color			color;
	double			factor_z;
	void			*img_ptr;
	char			*data;
	int				height;
	int				width;
	int				bpp;
	int				bpl;
	int				endian;
	double			spacing;
	double			save_spacing;
}					t_img;

typedef	struct		s_win
{
	void			*win_ptr;
	int				widht;
	int				height;
}					t_win;

typedef	struct		s_draw
{
	double			factor_v;
	t_point			vanish;
	t_point			centre;
	t_point			agl;
	t_color			color;
	t_point			offset;
	t_point			a;
	t_point			b;
	t_point			v;
	int				line_type;
	int				cone;
}					t_draw;

typedef	struct		s_wu
{
	t_point			d;
	t_point			end;
	t_point			pxl1;
	t_point			pxl2;
	int				steep;
	double			gradient;
	double			gap;
	double			intery;
}					t_wu;

typedef struct		s_prm
{
	t_draw			draw;
	t_map			map;
	t_img			img;
	t_win			win;
	t_wu			wu;
	void			*mlx_ptr;
}					t_prm;

typedef struct		s_tip
{
	int				width;
	int				height;
	void			*img_ptr;
}					t_tip;

void				print_cmd(void);
void				free_map(int **map);
void				clean_out(t_prm *prm);
void				window(char *title, t_prm *prm);
void				ft_free_buff(char **buff);
void				ft_error(char *str);
size_t				ft_split_n_convert(char *s, char c, int **tab);
void				print_array(int	**array, int size);
int					get_map(char *file, int ***tab, size_t *size);
int					get_prm(t_prm **prm, int **tab, int size);
int					refresh(t_prm *prm);
int					init(t_prm *prm, int **tab, int size);
int					rotation(t_prm *prm);
int					draw_line(t_prm *prm);
int					draw(t_prm *prm);
int					wu_line(t_prm *prm);
void				conique(t_prm *prm);
void				reset(t_prm *prm);
void				key_angle(int keycode, t_prm *prm);
int					key_press(int keycode, t_prm *prm);
#endif
