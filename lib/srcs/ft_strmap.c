/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 14:54:27 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/29 22:40:29 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*buff;
	int		i;

	i = 0;
	if (!s)
		return (NULL);
	if (!(buff = ft_strnew(ft_strlen(s))))
		return (0);
	while (s[i])
	{
		buff[i] = f(s[i]);
		i++;
	}
	return (buff);
}
