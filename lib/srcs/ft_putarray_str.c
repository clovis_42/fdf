/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putarray_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 20:19:25 by clmenega          #+#    #+#             */
/*   Updated: 2019/05/02 18:46:05 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putarray_str(char **array)
{
	if (!array)
		return ;
	while (*array)
	{
		ft_putstr(*array++);
		ft_putchar('\n');
	}
}
