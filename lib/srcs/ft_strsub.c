/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:10:30 by clmenega          #+#    #+#             */
/*   Updated: 2019/05/05 16:31:34 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*buff;

	if (!s)
		return (NULL);
	if (!(buff = (char*)ft_memalloc(len * sizeof(char) + 1)))
		return (NULL);
	ft_strncpy(buff, &s[start], len);
	buff[start + len] = '\0';
	return (buff);
}
