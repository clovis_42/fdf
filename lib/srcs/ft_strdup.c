/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 22:10:57 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/28 14:25:13 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*buff;
	size_t	len;

	len = ft_strlen(s1);
	if ((buff = (char*)ft_memalloc((len + 1) * sizeof(*s1))))
	{
		ft_memcpy(buff, s1, (len + 1));
		return (buff);
	}
	return (NULL);
}
