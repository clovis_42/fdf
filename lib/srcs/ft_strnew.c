/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 14:16:22 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/29 16:59:22 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*buff;

	if (!(buff = (char*)ft_memalloc((size + 1) * sizeof(char))))
		return (NULL);
	return (ft_memset(buff, '\0', size + 1));
}
