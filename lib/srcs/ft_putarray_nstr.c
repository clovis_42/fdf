/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putarray_nstr.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 20:25:11 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/28 14:18:02 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_putarray_nstr(char **array, size_t n)
{
	size_t	i;

	i = 0;
	while (*array[i] && i < n - 1)
	{
		ft_putstr(array[i]);
		ft_putchar('\n');
		i++;
	}
	if (i == n - 1)
		return (1);
	return (0);
}
