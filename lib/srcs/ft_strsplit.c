/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:46:16 by clmenega          #+#    #+#             */
/*   Updated: 2019/07/22 21:10:19 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_free_tab(char **tab)
{
	int i;

	i = 0;
	while (tab[i])
	{
		ft_strdel(&tab[i]);
		i++;
	}
}

static int	ft_count_w(char const *s, char c)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (i < (int)ft_strlen(s))
	{
		while (s[i] && s[i] == c)
			i++;
		if (s[i] && s[i] != c)
		{
			while (s[i] && s[i] != c)
				i++;
			count++;
		}
	}
	return (count);
}

static char	*ft_nextstr(const char *str, char c, int *index)
{
	int		i;
	char	*buff;

	i = *index;
	while (str[i] == c)
		i++;
	*index = i;
	while (str[i] != '\0' && str[i] != c)
		i++;
	if (!(buff = ft_strsub(str, *index, (i - *index))))
		return (NULL);
	*index = i;
	return (buff);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**ret;
	int		i;
	int		y;
	int		count;

	if (!s)
		return (NULL);
	count = ft_count_w(s, c);
	y = 0;
	i = 0;
	if (!(ret = (char**)ft_memalloc((count + 1) * sizeof(char*))))
		return (NULL);
	while (y < count)
	{
		if (!(ret[y] = ft_nextstr(s, c, &i)))
		{
			ft_free_tab(ret);
			return (NULL);
		}
		y++;
	}
	ret[y] = NULL;
	return (ret);
}
