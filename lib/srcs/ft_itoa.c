/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 22:36:08 by clmenega          #+#    #+#             */
/*   Updated: 2019/05/03 18:27:42 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	char	*buff;
	int		i;
	int		s;

	s = 0;
	if (n < 0)
		s = 1;
	i = ft_sizenbr(n);
	if (!(buff = (char*)ft_memalloc(i * sizeof(char) + 1)))
		return (NULL);
	buff[i] = '\0';
	i--;
	if (s == 1)
		buff[0] = '-';
	else
		n = -n;
	while (i >= s)
	{
		buff[i] = -(n % 10) + '0';
		n /= 10;
		i--;
	}
	return (buff);
}
