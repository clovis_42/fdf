/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstcat_str.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/04 19:55:49 by clmenega          #+#    #+#             */
/*   Updated: 2019/05/04 22:35:13 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t		count_size(t_list *lst)
{
	size_t	ret;

	ret = 0;
	while (lst)
	{
		ret += lst->content_size;
		lst = lst->next;
	}
	return (ret);
}

char				*ft_lstcat_str(t_list *lst)
{
	size_t	size;
	char	*str;

	size = count_size(lst);
	if (!(str = ft_memalloc(sizeof(char) * size + 1)))
		return (NULL);
	str = NULL;
	while (lst)
	{
		str = ft_strncat(str, lst->content, lst->content_size);
		lst = lst->next;
	}
	return (str);
}
