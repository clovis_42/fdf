/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/23 13:54:27 by clmenega          #+#    #+#             */
/*   Updated: 2019/07/28 19:36:56 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_fdlist		*ft_fdlstnew(int fd, int mode, t_fdlist *rm)
{
	t_fdlist	*list;

	if ((mode == 1) && (list = (t_fdlist*)ft_memalloc(sizeof(t_fdlist))))
	{
		list->content = NULL;
		list->content_size = 0;
		list->next = NULL;
		list->fd = fd;
		return (list);
	}
	if (mode == 2)
	{
		while (rm)
		{
			list = rm;
			rm = rm->next;
			list->content ? free(list->content) : 0;
			list->content = NULL;
			list ? free(list) : 0;
			list = NULL;
		}
	}
	return (NULL);
}

static t_fdlist		*ft_fdptr(t_fdlist *lst, const int fd, int mode)
{
	t_fdlist	*tmp;

	tmp = lst;
	while (tmp && mode == 1)
	{
		if (tmp->fd == fd)
			return (tmp);
		tmp = tmp->next;
	}
	if (mode == 2)
	{
		while (tmp)
		{
			if (tmp->fd != -1)
				return (tmp);
			tmp = tmp->next;
		}
	}
	return (NULL);
}

static	char		*ft_complet(char *tmp, int index, t_fdlist *lst)
{
	size_t	size;
	char	*buff;

	if (((buff = (char*)ft_memchr(tmp, '\n', index + 1)) != NULL)
			&& (index - (size = buff - tmp)) != 0)
	{
		if (!(lst->content = (char*)ft_memalloc(index - size)))
			return (NULL);
		ft_memcpy(lst->content, &tmp[size + 1], index - size);
		lst->content_size = index - size - 1;
	}
	else
	{
		size = index;
		lst->content_size = 0;
		lst->content = NULL;
	}
	ft_memcpy(buff = ft_memalloc(size + 1), tmp, size);
	buff[size] = '\0';
	tmp ? free(tmp) : 0;
	tmp = NULL;
	return (buff);
}

static	int			ft_read_line(t_fdlist *lst, const int fd, char **line)
{
	char	*buff;
	char	*tmp;
	int		index;
	int		rbit;

	rbit = 1;
	tmp = lst->content;
	index = lst->content_size;
	while ((!tmp || ft_memchr(tmp, '\n', index + 1) == NULL) && rbit != 0)
	{
		if (!(buff = (char*)ft_memalloc(index + BUFF_SIZE + 1)))
			return (-1);
		tmp ? ft_memcpy(buff, tmp, index) : 0;
		rbit = read(fd, &buff[index], BUFF_SIZE);
		if (rbit == -1 || (rbit == 0 && lst->content_size == 0 && index == 0))
			buff ? free(buff) : 0;
		if (rbit == -1 || (rbit == 0 && lst->content_size == 0 && index == 0))
			return (rbit);
		tmp ? free(tmp) : 0;
		tmp = buff;
		index += BUFF_SIZE - (BUFF_SIZE - rbit);
	}
	*line = ft_complet(tmp, index, lst);
	return (1);
}

int					get_next_line(const int fd, char **line)
{
	static t_fdlist	*init = NULL;
	t_fdlist		*tmp;
	int				ret;

	if (fd < 0 || BUFF_SIZE < 1 || !line)
		return (-1);
	if (!init)
		init = ft_fdlstnew(fd, 1, NULL);
	tmp = init;
	if (!(tmp = ft_fdptr(init, fd, 1)))
	{
		tmp = init;
		init = ft_fdlstnew(fd, 1, NULL);
		if (init == NULL)
			return (-1);
		init->next = tmp;
		tmp = init;
	}
	ret = ft_read_line(tmp, tmp->fd, line);
	if (ret == 0 || ret == -1)
		tmp->fd = -1;
	if (!ft_fdptr(init, fd, 2))
		init = ft_fdlstnew(fd, 2, init);
	return (ret);
}
