/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 21:59:23 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/12 14:31:41 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int i;
	int signe;
	int result;

	signe = 1;
	i = 0;
	result = 0;
	while (ft_isspace(str[i]))
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			signe = -1;
		i++;
	}
	while (ft_isdigit(str[i]))
	{
		result += str[i] - '0';
		if (result < 1000000000 && (ft_isdigit(str[i + 1]) != 0))
			result *= 10;
		else if (signe == -1 && result == 2147483640 && str[i + 1] == '8')
			return (-2147483648);
		i++;
	}
	return (result * signe);
}
