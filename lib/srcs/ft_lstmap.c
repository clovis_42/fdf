/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/01 13:55:48 by clmenega          #+#    #+#             */
/*   Updated: 2019/05/04 14:43:08 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*ret;

	ret = lst;
	if (lst && f)
	{
		if (!(new = ft_memalloc(sizeof(t_list))))
			return (NULL);
		if (!(new->content = ft_memalloc(f(lst)->content_size)))
			return (NULL);
		ft_memcpy(new->content, f(lst)->content, f(lst)->content_size);
		new->content_size = f(lst)->content_size;
		ret = new;
		ret->next = ft_lstmap(lst->next, f);
	}
	return (ret);
}
