/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:36:21 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/29 22:43:24 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	unsigned int	i;
	unsigned int	y;
	char			*ret;

	if (!s)
		return (NULL);
	i = 0;
	y = (ft_strlen(s));
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	while ((s[y] == ' ' || s[y] == '\n' || s[y] == '\t' || s[y] == '\0')
		&& y != 0)
		y--;
	if (y == 0)
	{
		if (!(ret = ft_strdup("")))
			return (NULL);
	}
	else
	{
		if (!(ret = ft_strsub(s, i, y - i + 1)))
			return (NULL);
	}
	return (ret);
}
