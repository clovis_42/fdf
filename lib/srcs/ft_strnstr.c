/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 18:49:42 by clmenega          #+#    #+#             */
/*   Updated: 2019/05/02 17:27:06 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	y;

	i = 0;
	y = 0;
	if (len == 0 && *needle != '\0')
		return (NULL);
	if (*needle == '\0' && len == 0)
		return ((void*)haystack);
	while (haystack[i] != '\0' && i < len)
	{
		while (haystack[i] == needle[y] && needle[y] && haystack[i] && i < len)
		{
			y++;
			i++;
		}
		if (needle[y] == '\0')
			return (&((char*)haystack)[i - y]);
		else if (i < len)
			i = i - y;
		y = 0;
		i++;
	}
	return (NULL);
}
