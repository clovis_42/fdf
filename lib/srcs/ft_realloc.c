/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 21:04:13 by clmenega          #+#    #+#             */
/*   Updated: 2019/08/29 21:03:13 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *ptr, size_t size)
{
	void	*tmp;

	tmp = NULL;
	if (ptr == NULL)
	{
		if (!(tmp = (void*)ft_memalloc(size)))
			return (NULL);
	}
	else if (size == 0)
	{
		if (!(tmp = (void*)ft_memalloc(sizeof(ptr))))
			return (NULL);
	}
	else
	{
		if (!(tmp = (void*)ft_memalloc(sizeof(ptr) * size)))
			return (NULL);
	}
	ft_memdel(&ptr);
	return (tmp);
}
