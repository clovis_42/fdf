/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 15:02:51 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/29 21:53:06 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*buff;
	int		index;

	if (!s || !f)
		return (0);
	index = 0;
	if (!(buff = ft_strnew(ft_strlen(s))))
		return (0);
	while (s[index])
	{
		buff[index] = f(index, s[index]);
		index++;
	}
	return (buff);
}
