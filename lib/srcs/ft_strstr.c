/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 12:27:47 by clmenega          #+#    #+#             */
/*   Updated: 2019/04/29 22:42:51 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	int	i;
	int	y;

	i = 0;
	y = 0;
	if (ft_strlen(needle) == 0)
	{
		return ((char*)haystack);
	}
	while (haystack[i] != '\0')
	{
		while (haystack[i] == needle[y] && needle[y] != '\0')
		{
			y++;
			i++;
		}
		if (needle[y] == '\0')
			return (&((char*)haystack)[i - y]);
		else
			i = i - y;
		y = 0;
		i++;
	}
	return (NULL);
}
