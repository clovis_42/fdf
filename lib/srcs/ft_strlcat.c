/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 18:38:17 by clmenega          #+#    #+#             */
/*   Updated: 2019/05/01 17:42:49 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t i;
	size_t y;
	size_t ret;

	ret = ft_strlen(dst) + ft_strlen(src);
	i = ft_strlen(dst);
	y = 0;
	if (size <= ft_strlen(dst))
		return (ft_strlen(src) + size);
	while (src[y] != '\0' && i < size - 1)
	{
		dst[i] = src[y];
		i++;
		y++;
	}
	dst[i] = '\0';
	return (ret);
}
