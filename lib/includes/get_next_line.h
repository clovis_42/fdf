/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clmenega <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/04 18:30:42 by clmenega          #+#    #+#             */
/*   Updated: 2019/09/09 18:21:21 by clmenega         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE 32
# include "libft.h"
# include <unistd.h>
# include <stdlib.h>

int						get_next_line(const int fd, char **line);

typedef struct			s_fdlist
{
	char				*content;
	size_t				content_size;
	int					fd;
	struct s_fdlist		*next;
}						t_fdlist;

#endif
